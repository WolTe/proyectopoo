<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nombre', 
        'apellidos', 
        'cedula', 
        'direccion',
        'telefono', 
        'fecha_nacimiento', 
        'email'
    ];
}
