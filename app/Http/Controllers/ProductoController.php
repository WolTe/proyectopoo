<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tallerCorteDosPuntoUno()
    {
        $mes = 1;

        switch ($mes) {
            case 1:
                echo("El mes es enero");
                break;
            case 2:
                echo("El mes es febrero");
                break;
            case 10:
                echo("El mes es octubre");
                break;
            default:
                echo("El mes no es enero, febrero ni octubre");
                break;
        }
    }

    public function tallerCorteDosPuntoDos()
    {
        $tipoMotor = 3;

        switch ($tipoMotor) {
            case 0:
                echo("No hay establecido un valor definido para el tipo de bomba");
                break;
            case 1:
                echo("La bomba es una bomba de agua");
                break;
            case 2:
                echo("La bomba es una bomba de gasolina");
                break;
            case 3:
                echo("La bomba es una bomba de hormigón");
                break;
            case 4:
                    echo("La bomba es una bomba de pasta alimenticia");
                    break;
            default:
                echo("“No existe un valor válido para tipo de bomba");
                break;
        }
    }

    public function InicioProducto(Request $request)
    {
        // dd('Hola Mundo');
        $producto = Producto::all();
        // dd($producto);
        return view('productos.inicio')-> with('producto', $producto);
    }

    public function CrearProducto(Request $request) {
        $producto = Producto::all();
        return view('productos.crear')->with('producto', $producto);
    }

    public function GuardarProducto(Request $request) {
        $this->validate($request, [
            'nombre'  => 'required',
            'tipo'    => 'required',
            'estado'  => 'required',
            'precio'  => 'required'
        ]);

        $producto = new Producto;
        $producto->nombre  = $request->nombre;
        $producto->tipo    = $request->tipo;
        $producto->estado  = $request->estado;
        $producto->precio  = $request->precio;
        $producto->save();

        return redirect()->route('list.productos');
        
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
